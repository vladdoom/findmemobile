package com.findme.app_findme.adapter;

        import android.content.Context;
        import android.database.Cursor;
        import android.graphics.drawable.Drawable;
        import android.support.v4.content.ContextCompat;
        import android.util.Log;
        import android.view.LayoutInflater;
        import android.view.View;
        import android.view.ViewGroup;
        import android.widget.CursorAdapter;
        import android.widget.ImageView;
        import android.widget.TextView;

        import com.findme.app_findme.R;

        import static com.facebook.accountkit.internal.AccountKitController.getApplicationContext;

/**
 * Created by adminn on 24.10.2016.
 */

public class TodoCursorAdapter extends CursorAdapter {
    public TodoCursorAdapter(Context context, Cursor cursor) {
        super(context, cursor, 0);
    }
    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        Log.d("R","newView");
        return LayoutInflater.from(context).inflate(R.layout.item_todo, parent, false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) throws IllegalArgumentException{
        TextView listUser = (TextView) view.findViewById(R.id.listUser);
        ImageView imageView =(ImageView) view.findViewById(R.id.tvHead);
       TextView listAdress = (TextView) view.findViewById(R.id.listAdress);
        TextView listDate = (TextView) view.findViewById(R.id.listDate);
        TextView listCategory = (TextView) view.findViewById(R.id.listCategory);

        // Extract properties from cursor
        String body = cursor.getString(cursor.getColumnIndexOrThrow("summery"));
        //int priority = cursor.getInt(cursor.getColumnIndexOrThrow("_id"));
        String head = cursor.getString(cursor.getColumnIndexOrThrow("name"));
        // Populate fields with extracted properties
        listAdress.setText(body );
        listUser.setText(head );
        listDate.setText("(10.00) 12.11.2016");
        listCategory.setText("Футбол");
        Drawable drawable;
        if(android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP){
            drawable = context.getResources().getDrawable(R.drawable.cat1, context.getTheme());
        } else {
            drawable = context.getResources().getDrawable(R.drawable.cat1);
        }
        imageView.setImageDrawable(drawable);
        Log.d("Q","bindView");
    }
}

