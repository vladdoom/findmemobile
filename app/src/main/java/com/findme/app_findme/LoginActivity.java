package com.findme.app_findme;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageInstaller;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphRequestAsyncTask;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.LoggingBehavior;
import com.facebook.Profile;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.appindexing.Thing;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.places.internal.PlaceOpeningHoursEntity;
import com.vk.sdk.VKAccessToken;
import com.vk.sdk.VKCallback;
import com.vk.sdk.VKScope;
import com.vk.sdk.VKSdk;
import com.vk.sdk.VKUIHelper;
import com.vk.sdk.api.VKApi;
import com.vk.sdk.api.VKApiConst;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKParameters;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;
import com.vk.sdk.dialogs.VKCaptchaDialog;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPostHC4;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;

import javax.net.ssl.HttpsURLConnection;

import static com.findme.app_findme.R.id.textView;

public class LoginActivity extends AppCompatActivity {

    private boolean flagVk=false;
    private boolean flagFacebook=false;
    private boolean flagOneReg = true;
    private static String sTokenKey = "VK_ACCESS_TOKEN";
    private static String[] sMyScope = new String[]{VKScope.FRIENDS, VKScope.WALL, VKScope.PHOTOS, VKScope.NOHTTPS};
    private CallbackManager callbackManager;
    private Profile profile;
    private LoginButton loginButton;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        VKSdk.initialize(getApplicationContext());
        FacebookSdk.sdkInitialize(getApplicationContext(), new FacebookSdk.InitializeCallback() {
            @Override
            public void onInitialized() {
                if(AccessToken.getCurrentAccessToken() == null){
                    Log.d("START","not logged in yet");
                } else {
                    Log.d("START","Logged in");
                }
            }
        });

        // FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);

        setContentView(R.layout.activity_login);

        FacebookSdk.addLoggingBehavior(LoggingBehavior.REQUESTS);

        Button vk_button = (Button) findViewById(R.id.button_vk);
        vk_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            if (isNetworkAvailable()) {
                login_vk();
            }
              /*  if (flagOneReg && flagFacebook) {
            flagOneReg = false;
            Timer timer = new Timer(true);
            try {
            //
                Log.d("Timer","Start timer");
                synchronized(timer){
                timer.wait(5000);}
                Log.d("Timer","End timer");
                if (flagFacebook){}else{flagOneReg=true;}

            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }*/
            }
        });
        //    hashKey();


         callbackManager = new CallbackManager.Factory().create();

        loginButton = (LoginButton) findViewById(R.id.login_button);

        loginButton.setReadPermissions(Arrays.asList(
                "public_profile", "email", "user_birthday", "user_friends"));

        // If using in a fragment


        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               if( isNetworkAvailable()){facebookGeristration();}
            }
        });

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();



    }

    public void ffff() {
try {
  //  profile = Profile.getCurrentProfile();
   // Log.d("Profile", profile.getFirstName() + "_" + profile.getLastName() + "_" + profile.getMiddleName() + "_" + profile.getId() + "_" + profile.getName() + "_" + profile.toString() + "_" + profile.describeContents() + "_" + profile.getLinkUri() + "_" + profile.getProfilePictureUri(64, 64));
}catch(Exception e){e.printStackTrace();}
    try {
    new GraphRequest(
            AccessToken.getCurrentAccessToken(),
            "/{user-id}",
            null,
            HttpMethod.GET,
            new GraphRequest.Callback() {
                public void onCompleted(GraphResponse response) {
            /* handle the result */
                    Log.d("", response.toString());
                }
            }
    ).executeAsync();
}catch (Exception e){e.printStackTrace();}
      //  Toast.makeText(this, "You are registration!", Toast.LENGTH_LONG).show();
    }



    private void hashKey() {
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "com.findme.app_findme",  //Replace your package name here
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());

                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }
    }

    private void login_vk() {
        Log.d("VK", "Go to Login");
        VKSdk.login(this, sMyScope);

    }

    private void goToMap() {
        Log.d("Table", "Go to table");

        Intent intent = new Intent(this, TableActivity.class);
        startActivity(intent);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (!VKSdk.onActivityResult(requestCode, resultCode, data, new VKCallback<VKAccessToken>() {
            @Override
            public void onResult(VKAccessToken res) {
// Пользователь успешно авторизовался
               // flagVk=true;
                goToMap();
                Log.d("VK", "Authorization");
                Log.d("VKVK",res.toString());
                finish();
            }

            @Override
            public void onError(VKError error) {
// Произошла ошибка авторизации (например, пользователь запретил авторизацию)
                Log.d("VK", "False");
                toastCancelError();
            }
        }))
        {
            super.onActivityResult(requestCode, resultCode, data);
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }


    private void setProfileToView(JSONObject jsonObject) {
        try {
            Log.d("Facebook",jsonObject.toString());



        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    public Action getIndexApiAction() {
        Thing object = new Thing.Builder()
                .setName("Login Page") // TODO: Define a title for the content shown.
                // TODO: Make sure this auto-generated URL is correct.
                .setUrl(Uri.parse("http://[ENTER-YOUR-URL-HERE]"))
                .build();
        return new Action.Builder(Action.TYPE_VIEW)
                .setObject(object)
                .setActionStatus(Action.STATUS_TYPE_COMPLETED)
                .build();
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        AppIndex.AppIndexApi.start(client, getIndexApiAction());
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        AppIndex.AppIndexApi.end(client, getIndexApiAction());
        client.disconnect();
    }




public void requestVk(){
    VKRequest request = VKApi.users().get(VKParameters.from(VKApiConst.FIELDS, "id,first_name,last_name,bdate,sex"));;
    request.executeWithListener(new VKRequest.VKRequestListener() {
        @Override
        public void onComplete(VKResponse response) {
//Do complete stuff
            Log.d("VKVK", response.toString());
            JSONObject jsonObject = response.json;
            Log.d("VKVK", jsonObject.toString());

        }
        @Override
        public void onError(VKError error) {
//Do error stuff
        }
        @Override
        public void attemptFailed(VKRequest request, int attemptNumber, int totalAttempts) {
//I don't really believe in progress
        }
    });

     request = VKApi.friends().get(VKParameters.from(VKApiConst.FIELDS, "id,first_name,last_name,bdate"));;
    request.executeWithListener(new VKRequest.VKRequestListener() {
        @Override
        public void onComplete(VKResponse response) {
//Do complete stuff
            Log.d("VKVK", response.toString());
            JSONObject jsonObject = response.json;
            Log.d("VKVK", jsonObject.toString());

        }
        @Override
        public void onError(VKError error) {
//Do error stuff
        }
        @Override
        public void attemptFailed(VKRequest request, int attemptNumber, int totalAttempts) {
//I don't really believe in progress
        }
    });

}


    public void requestFB(){
if(AccessToken.getCurrentAccessToken()!=null) {
    GraphRequest request = GraphRequest.newMeRequest(
            AccessToken.getCurrentAccessToken(),
            new GraphRequest.GraphJSONObjectCallback() {
                @Override
                public void onCompleted(JSONObject object, GraphResponse response) {
                    Log.v("LoginActivity", response.toString());

                    // Application code
                    try {
                        String email = object.getString("email");
                        String birthday = object.getString("birthday"); // 01/31/1980 format
                    } catch (Exception j) {
                        j.printStackTrace();
                    }
                }
            });
    Bundle parameters = new Bundle();
    parameters.putString("fields", "id,name,email,gender,birthday");
    request.setParameters(parameters);
    request.executeAsync();
}else{Log.d("Acc","Fall");}
    }


    public boolean isNetworkAvailable() {
           ConnectivityManager cm = (ConnectivityManager)
                  getSystemService(Context.CONNECTIVITY_SERVICE);
          NetworkInfo networkInfo = cm.getActiveNetworkInfo();
       //  if no network is available networkInfo will be null
       //  otherwise check if we are connected
          if (networkInfo != null && networkInfo.isConnected()) {
             // Toast.makeText(this, "Network is avaiable", Toast.LENGTH_LONG).show();
              return true;        }
        Toast.makeText(this, "No connect to network", Toast.LENGTH_LONG).show();
        return false;

    }

    private void facebookGeristration(){
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {

            @Override
            public void onSuccess(LoginResult loginResult) {
                flagFacebook=true;
                Log.d("R", "Facebook Ok Log");
                goToMap();
    finish();

              /*  GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {
                                Log.v("LoginActivity", response.toString());

                                // Application code
                                try {
                                    String email = object.getString("email");
                                    String birthday = object.getString("birthday"); // 01/31/1980 format
                                }catch (JSONException j){j.printStackTrace();}
                            }
                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,email,gender,birthday");
                request.setParameters(parameters);
                request.executeAsync();

                ffff();*/
            }

            @Override
            public void onCancel() {
                toastCancelError();
            }

            @Override
            public void onError(FacebookException error) {
                toastCancelError();
                Log.d("R", "Facebook On error");
            }
        });
    }
    private void toastCancelError() {
        Toast.makeText(this, "Cancel registration" , Toast.LENGTH_LONG).show();
    }
}
