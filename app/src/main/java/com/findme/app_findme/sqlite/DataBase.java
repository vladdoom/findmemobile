package com.findme.app_findme.sqlite;

/**
 * Created by adminn on 21.10.2016.
 */

        import android.content.Context;
        import android.database.sqlite.SQLiteDatabase;
        import android.database.sqlite.SQLiteOpenHelper;


public class DataBase extends SQLiteOpenHelper {
    public DataBase(Context context) {
        super(context, "_MyDataBase", null, 5);
    }

    public void onCreate(SQLiteDatabase db)
    {

        db.execSQL
                ("create table Category ("
                        + "id integer primary key autoincrement,"
                        + "name text,"
                        + "sub_name text"
                        +");");

        db.execSQL
                ("create table Own ("
                        + "id integer primary key autoincrement,"
                        + "event_id text,"
                        + "accept text,"
                        + "reject text"
                        //  + "foreign key (event_id) references Category(event_id)"
                        +");");
        db.execSQL
                ("create table Event ("
                        + "_id integer primary key autoincrement,"
                        //      + "event_id text,"
                        //      + "date text,"
                        //      + "time text,"
                        //      + "place text,"
                        //      + "address text,"
                        + "name text,"
                        + "summery text"
                        //      + "site text,"
                        //      + "own text,"
                        //      + "new text,"
                        //     + "private text,"
                        //     + "user_id text,"
                        //     + "category_id text"
                        // + "foreign key (user_id) references User(user_id),"
                        // + "foreign key (category_id) references Category(id)"
                        +");");

        db.execSQL
                ("create table User ("
                        + "id integer primary key autoincrement,"
                        + "user_id text,"
                        + "name text,"
                        + "gender text,"
                        + "age text,"
                        + "ava text,"
                        + "cite text"
                        +");");

        db.execSQL
                ("create table Info ("
                        + "id integer primary key autoincrement,"
                        + "last_event_id text,"
                        + "last_category_id text,"
                        + "last_user_id text,"
                        + "last_change_user text,"
                        + "own_user text"

                        +");");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS Event");
        db.execSQL("DROP TABLE IF EXISTS Category");
        db.execSQL("DROP TABLE IF EXISTS Own");
        db.execSQL("DROP TABLE IF EXISTS Info");
        db.execSQL("DROP TABLE IF EXISTS User");

        onCreate(db);
    }}
