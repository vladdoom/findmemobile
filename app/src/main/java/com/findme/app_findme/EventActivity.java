package com.findme.app_findme;

import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.LikeView;
import com.facebook.share.widget.ShareButton;

public class EventActivity extends AppCompatActivity {
private ShareLinkContent content;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event);
        Button button = (Button)findViewById(R.id.shareFacebook);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sharefacebook();
            }
        });

        ShareButton shareButton = (ShareButton)findViewById(R.id.fb_share_button);
        shareButton.setShareContent(content);

        //create like view
//        LikeView likeView = (LikeView) findViewById(R.id.like_view);
 //       likeView.setContentDescription(content.getContentDescription());


    }

    private void sharefacebook() {
     //   contentURL — публикуемая ссылка;
      //  contentTitle — заголовок материалов в ссылке;
      //  imageURL — URL-адрес миниатюры, которая будет отображаться в публикации;
      //  contentDescription — описание материалов (обычно 2-4 предложения).

         content = new ShareLinkContent.Builder()
                .setContentTitle("Bla Bla Bla")
                .setContentDescription("Bla Bla Bla Bla Bla Bla Bla Bla").build();


              //  .setContentUrl(Uri.parse("https://developers.facebook.com"))
               // .build();

    }


}
