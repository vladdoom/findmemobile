package com.findme.app_findme;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.findme.app_findme.adapter.TodoCursorAdapter;
import com.findme.app_findme.sqlite.DataBase;
import com.vk.sdk.VKAccessToken;
import com.vk.sdk.VKSdk;
import com.vk.sdk.util.VKUtil;

public class TableActivity extends AppCompatActivity {
    private boolean flagVkReg=false;
    private boolean flagFacebookReg=false;

    private Intent intent;
    private DataBase dbHelper;
    private SQLiteDatabase db;
    private ContentValues  cv = new ContentValues();
    private Cursor cursor ;
    public void openDB() {
        dbHelper = new DataBase(this);
        db=dbHelper.getWritableDatabase();
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_table);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        openDB();
        viewList();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_list, menu);
        return true;
    }





    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement


        if (id == R.id.action_addevent) {
            addEvent();

            return true;
        }
        if (id == R.id.action_view) {

            viewList();
            return true;
        }
        if (id == R.id.action_map) {
            intent = new Intent(this,MainActivity.class);
            startActivity(intent);

            return true;
        }
        if (id == R.id.action_event) {
            intent = new Intent(this,EventActivity.class);
            startActivity(intent);

            return true;
        }
        if (id == R.id.action_logOut) {
            if (flagFacebookReg){
                LoginManager.getInstance().logOut();
            }
            if (flagVkReg){
                VKSdk.logout();
            }
            goToLoginActivity();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void addEvent(){
        String  [] name ={"Інна Ціх 21р","Ігор Петров 26р","Бібліотека №7"};
        String [] summery ={"Львів, вул. Під дубом, 12",
                "Львів, пр. Чорновола, 26",
                "Львів, вул. Стефаника, 2"};

        for (int j=0;j<3;j++){
            cv.put("name",name[j]);
            cv.put("summery",summery[j]);
            try {
                db.insert("Event", null, cv);

            } catch (Exception e) {
                e.printStackTrace();
            }


        }
        Log.d("Help", "OK Data Base is save");

    }
    private void viewList(){
        cursor=db.query("Event",null,null,null,null,null,null);//new String[] {"name","summery"}
        if(cursor.moveToFirst()) {
           // Toast.makeText(getBaseContext(), cursor.getString(1)+cursor.getString(0), Toast.LENGTH_LONG).show();
        }else{Toast.makeText(getBaseContext(), "Error" , Toast.LENGTH_LONG).show();
        }
        // Find ListView to populate
        ListView lvItems = (ListView) findViewById(R.id.list1);
// Setup cursor adapter using cursor from last step
        try {
            TodoCursorAdapter todoAdapter = new TodoCursorAdapter(this, cursor);
            lvItems.setAdapter(todoAdapter);
        }catch (IllegalArgumentException i){
            Log.d("Q",i.getMessage());}
// Attach cursor adapter to the ListView

    }
    private void goMap() {
        Log.d("Map", "Go to map");

        Intent intent = new Intent(this, MapsActivity.class);
        startActivity(intent);
    }

    public void goToLoginActivity(){
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }
}
